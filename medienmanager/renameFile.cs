﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Medienmanager
{
    public partial class renameFile : Form
    {
        string currentPath = "", selectedItem = "", fileExtension="";
        private mediaManager parentForm = null;

        public renameFile()
        {
            InitializeComponent();
        }

        public renameFile(Form callingForm, string path, string item)
        {
           InitializeComponent();
           parentForm = callingForm as mediaManager;
           currentPath = path;
           selectedItem = item;
           fileExtension = Path.GetExtension(item);

           txtOldname.Text = selectedItem.Split('.')[0];
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            parentForm.renamefiles(currentPath + "\\" + txtOldname.Text + fileExtension, currentPath + "\\" + txtNewname.Text + fileExtension);
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
