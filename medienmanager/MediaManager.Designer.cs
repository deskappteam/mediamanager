﻿namespace Medienmanager
{
    partial class mediaManager
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mediaManager));
            System.Windows.Forms.TreeNode treeNode37 = new System.Windows.Forms.TreeNode("Audio");
            System.Windows.Forms.TreeNode treeNode38 = new System.Windows.Forms.TreeNode("Picture");
            System.Windows.Forms.TreeNode treeNode39 = new System.Windows.Forms.TreeNode("Text");
            System.Windows.Forms.TreeNode treeNode40 = new System.Windows.Forms.TreeNode("Video");
            System.Windows.Forms.TreeNode treeNode41 = new System.Windows.Forms.TreeNode("Andere");
            System.Windows.Forms.TreeNode treeNode42 = new System.Windows.Forms.TreeNode("Media Manager", new System.Windows.Forms.TreeNode[] {
            treeNode37,
            treeNode38,
            treeNode39,
            treeNode40,
            treeNode41});
            System.Windows.Forms.ListViewGroup listViewGroup1 = new System.Windows.Forms.ListViewGroup("ListViewGroup", System.Windows.Forms.HorizontalAlignment.Left);
            this.pnlPreview = new System.Windows.Forms.Panel();
            this.dateiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.öffnenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.schließenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bearbeitenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kopierenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.löschenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annotierenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.medientypToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textDokumenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.audioFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.videoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bilderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.andereToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.ilstTreeView = new System.Windows.Forms.ImageList(this.components);
            this.tvwMediaManager = new System.Windows.Forms.TreeView();
            this.btnUpload = new System.Windows.Forms.Button();
            this.ilstFileAndIcon = new System.Windows.Forms.ImageList(this.components);
            this.btnAddFolder = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnMove = new System.Windows.Forms.Button();
            this.lsvFiles = new System.Windows.Forms.ListView();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.lblPath = new System.Windows.Forms.Label();
            this.btnCopy = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.btnZoomout = new System.Windows.Forms.Button();
            this.btnRename = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtSearchFile = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlPathImg = new System.Windows.Forms.Panel();
            this.lblPathImg = new System.Windows.Forms.Label();
            this.btnZoomin = new System.Windows.Forms.Button();
            this.lblDocPreview = new System.Windows.Forms.Label();
            this.menuStrip2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlPathImg.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlPreview
            // 
            this.pnlPreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPreview.Location = new System.Drawing.Point(924, 73);
            this.pnlPreview.Name = "pnlPreview";
            this.pnlPreview.Size = new System.Drawing.Size(369, 620);
            this.pnlPreview.TabIndex = 4;
            // 
            // dateiToolStripMenuItem
            // 
            this.dateiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.öffnenToolStripMenuItem,
            this.schließenToolStripMenuItem});
            this.dateiToolStripMenuItem.Name = "dateiToolStripMenuItem";
            this.dateiToolStripMenuItem.Size = new System.Drawing.Size(50, 21);
            this.dateiToolStripMenuItem.Text = "Datei";
            // 
            // öffnenToolStripMenuItem
            // 
            this.öffnenToolStripMenuItem.Name = "öffnenToolStripMenuItem";
            this.öffnenToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.öffnenToolStripMenuItem.Text = "Laden";
            this.öffnenToolStripMenuItem.Click += new System.EventHandler(this.öffnenToolStripMenuItem_Click);
            // 
            // schließenToolStripMenuItem
            // 
            this.schließenToolStripMenuItem.Name = "schließenToolStripMenuItem";
            this.schließenToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.schließenToolStripMenuItem.Text = "Schließen";
            this.schließenToolStripMenuItem.Click += new System.EventHandler(this.schließenToolStripMenuItem_Click);
            // 
            // bearbeitenToolStripMenuItem
            // 
            this.bearbeitenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verToolStripMenuItem,
            this.kopierenToolStripMenuItem,
            this.löschenToolStripMenuItem,
            this.annotierenToolStripMenuItem});
            this.bearbeitenToolStripMenuItem.Name = "bearbeitenToolStripMenuItem";
            this.bearbeitenToolStripMenuItem.Size = new System.Drawing.Size(82, 21);
            this.bearbeitenToolStripMenuItem.Text = "Bearbeiten";
            // 
            // verToolStripMenuItem
            // 
            this.verToolStripMenuItem.Name = "verToolStripMenuItem";
            this.verToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.verToolStripMenuItem.Text = "Verschieben";
            this.verToolStripMenuItem.Click += new System.EventHandler(this.verToolStripMenuItem_Click);
            // 
            // kopierenToolStripMenuItem
            // 
            this.kopierenToolStripMenuItem.Name = "kopierenToolStripMenuItem";
            this.kopierenToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.kopierenToolStripMenuItem.Text = "Kopieren";
            this.kopierenToolStripMenuItem.Click += new System.EventHandler(this.kopierenToolStripMenuItem_Click);
            // 
            // löschenToolStripMenuItem
            // 
            this.löschenToolStripMenuItem.Name = "löschenToolStripMenuItem";
            this.löschenToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.löschenToolStripMenuItem.Text = "Löschen";
            this.löschenToolStripMenuItem.Click += new System.EventHandler(this.löschenToolStripMenuItem_Click);
            // 
            // annotierenToolStripMenuItem
            // 
            this.annotierenToolStripMenuItem.Name = "annotierenToolStripMenuItem";
            this.annotierenToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.annotierenToolStripMenuItem.Text = "Annotieren";
            this.annotierenToolStripMenuItem.Click += new System.EventHandler(this.annotierenToolStripMenuItem_Click);
            // 
            // medientypToolStripMenuItem
            // 
            this.medientypToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.textDokumenteToolStripMenuItem,
            this.audioFilesToolStripMenuItem,
            this.videoToolStripMenuItem,
            this.bilderToolStripMenuItem,
            this.andereToolStripMenuItem});
            this.medientypToolStripMenuItem.Name = "medientypToolStripMenuItem";
            this.medientypToolStripMenuItem.Size = new System.Drawing.Size(80, 21);
            this.medientypToolStripMenuItem.Text = "Medienart";
            // 
            // textDokumenteToolStripMenuItem
            // 
            this.textDokumenteToolStripMenuItem.Name = "textDokumenteToolStripMenuItem";
            this.textDokumenteToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.textDokumenteToolStripMenuItem.Text = "Text-Dokumente";
            this.textDokumenteToolStripMenuItem.Click += new System.EventHandler(this.textDokumenteToolStripMenuItem_Click);
            // 
            // audioFilesToolStripMenuItem
            // 
            this.audioFilesToolStripMenuItem.Name = "audioFilesToolStripMenuItem";
            this.audioFilesToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.audioFilesToolStripMenuItem.Text = "Audio Files";
            this.audioFilesToolStripMenuItem.Click += new System.EventHandler(this.audioFilesToolStripMenuItem_Click);
            // 
            // videoToolStripMenuItem
            // 
            this.videoToolStripMenuItem.Name = "videoToolStripMenuItem";
            this.videoToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.videoToolStripMenuItem.Text = "Video";
            this.videoToolStripMenuItem.Click += new System.EventHandler(this.videoToolStripMenuItem_Click);
            // 
            // bilderToolStripMenuItem
            // 
            this.bilderToolStripMenuItem.Name = "bilderToolStripMenuItem";
            this.bilderToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.bilderToolStripMenuItem.Text = "Bilder";
            this.bilderToolStripMenuItem.Click += new System.EventHandler(this.bilderToolStripMenuItem_Click);
            // 
            // andereToolStripMenuItem
            // 
            this.andereToolStripMenuItem.Name = "andereToolStripMenuItem";
            this.andereToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.andereToolStripMenuItem.Text = "Andere";
            this.andereToolStripMenuItem.Click += new System.EventHandler(this.andereToolStripMenuItem_Click);
            // 
            // menuStrip2
            // 
            this.menuStrip2.BackColor = System.Drawing.Color.LightGray;
            this.menuStrip2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dateiToolStripMenuItem,
            this.bearbeitenToolStripMenuItem,
            this.medientypToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip2.Size = new System.Drawing.Size(1296, 25);
            this.menuStrip2.TabIndex = 1;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // ilstTreeView
            // 
            this.ilstTreeView.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilstTreeView.ImageStream")));
            this.ilstTreeView.TransparentColor = System.Drawing.Color.Transparent;
            this.ilstTreeView.Images.SetKeyName(0, "networking.png");
            this.ilstTreeView.Images.SetKeyName(1, "audio.png");
            this.ilstTreeView.Images.SetKeyName(2, "file.png");
            this.ilstTreeView.Images.SetKeyName(3, "folder.png");
            this.ilstTreeView.Images.SetKeyName(4, "picture.png");
            this.ilstTreeView.Images.SetKeyName(5, "video.png");
            this.ilstTreeView.Images.SetKeyName(6, "file(2).png");
            this.ilstTreeView.Images.SetKeyName(7, "back-icon.png");
            this.ilstTreeView.Images.SetKeyName(8, "forward-icon.png");
            // 
            // tvwMediaManager
            // 
            this.tvwMediaManager.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvwMediaManager.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tvwMediaManager.Indent = 20;
            this.tvwMediaManager.ItemHeight = 27;
            this.tvwMediaManager.Location = new System.Drawing.Point(6, 73);
            this.tvwMediaManager.Name = "tvwMediaManager";
            treeNode37.ImageIndex = 1;
            treeNode37.Name = "nodeAudio";
            treeNode37.Text = "Audio";
            treeNode38.ImageIndex = 4;
            treeNode38.Name = "nodePicture";
            treeNode38.Text = "Picture";
            treeNode39.ImageIndex = 2;
            treeNode39.Name = "nodeText";
            treeNode39.Text = "Text";
            treeNode40.ImageIndex = 5;
            treeNode40.Name = "nodeVideo";
            treeNode40.Text = "Video";
            treeNode41.Name = "nodeMix";
            treeNode41.Text = "Andere";
            treeNode42.ImageIndex = 0;
            treeNode42.Name = "mediaManager";
            treeNode42.Text = "Media Manager";
            this.tvwMediaManager.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode42});
            this.tvwMediaManager.Size = new System.Drawing.Size(191, 620);
            this.tvwMediaManager.TabIndex = 0;
            this.tvwMediaManager.TabStop = false;
            this.tvwMediaManager.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvwMediaManager_AfterSelect);
            // 
            // btnUpload
            // 
            this.btnUpload.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnUpload.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpload.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnUpload.Image = global::Medienmanager.Properties.Resources.upload_2_;
            this.btnUpload.Location = new System.Drawing.Point(263, 35);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(31, 28);
            this.btnUpload.TabIndex = 5;
            this.btnUpload.UseVisualStyleBackColor = false;
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // ilstFileAndIcon
            // 
            this.ilstFileAndIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilstFileAndIcon.ImageStream")));
            this.ilstFileAndIcon.TransparentColor = System.Drawing.Color.Transparent;
            this.ilstFileAndIcon.Images.SetKeyName(0, "folder-symbol.png");
            this.ilstFileAndIcon.Images.SetKeyName(1, "upload.png");
            this.ilstFileAndIcon.Images.SetKeyName(2, "garbage.png");
            // 
            // btnAddFolder
            // 
            this.btnAddFolder.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnAddFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddFolder.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddFolder.Image = ((System.Drawing.Image)(resources.GetObject("btnAddFolder.Image")));
            this.btnAddFolder.Location = new System.Drawing.Point(391, 35);
            this.btnAddFolder.Name = "btnAddFolder";
            this.btnAddFolder.Size = new System.Drawing.Size(31, 28);
            this.btnAddFolder.TabIndex = 6;
            this.btnAddFolder.UseVisualStyleBackColor = false;
            this.btnAddFolder.Click += new System.EventHandler(this.btnAddFolder_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(464, 35);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(31, 28);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnMove
            // 
            this.btnMove.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnMove.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMove.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnMove.Image = ((System.Drawing.Image)(resources.GetObject("btnMove.Image")));
            this.btnMove.Location = new System.Drawing.Point(533, 35);
            this.btnMove.Name = "btnMove";
            this.btnMove.Size = new System.Drawing.Size(31, 28);
            this.btnMove.TabIndex = 8;
            this.btnMove.UseVisualStyleBackColor = false;
            this.btnMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // lsvFiles
            // 
            this.lsvFiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            listViewGroup1.Header = "ListViewGroup";
            listViewGroup1.Name = "listViewGroup0";
            this.lsvFiles.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup1});
            this.lsvFiles.LargeImageList = this.ilstFileAndIcon;
            this.lsvFiles.Location = new System.Drawing.Point(203, 104);
            this.lsvFiles.Name = "lsvFiles";
            this.lsvFiles.Size = new System.Drawing.Size(712, 589);
            this.lsvFiles.SmallImageList = this.ilstFileAndIcon;
            this.lsvFiles.TabIndex = 10;
            this.lsvFiles.UseCompatibleStateImageBehavior = false;
            this.lsvFiles.View = System.Windows.Forms.View.Tile;
            this.lsvFiles.Click += new System.EventHandler(this.lsvFiles_Click);
            this.lsvFiles.DoubleClick += new System.EventHandler(this.listView1_DoubleClick);
            // 
            // btnLeft
            // 
            this.btnLeft.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLeft.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnLeft.Image = ((System.Drawing.Image)(resources.GetObject("btnLeft.Image")));
            this.btnLeft.Location = new System.Drawing.Point(203, 35);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(31, 28);
            this.btnLeft.TabIndex = 11;
            this.btnLeft.UseVisualStyleBackColor = false;
            this.btnLeft.Click += new System.EventHandler(this.btnFolderUp_Click);
            // 
            // btnRight
            // 
            this.btnRight.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRight.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRight.Image = ((System.Drawing.Image)(resources.GetObject("btnRight.Image")));
            this.btnRight.Location = new System.Drawing.Point(327, 35);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(31, 28);
            this.btnRight.TabIndex = 13;
            this.btnRight.UseVisualStyleBackColor = false;
            this.btnRight.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblPath
            // 
            this.lblPath.AutoSize = true;
            this.lblPath.BackColor = System.Drawing.Color.Transparent;
            this.lblPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPath.ForeColor = System.Drawing.Color.DimGray;
            this.lblPath.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lblPath.Location = new System.Drawing.Point(4, 4);
            this.lblPath.Name = "lblPath";
            this.lblPath.Size = new System.Drawing.Size(0, 13);
            this.lblPath.TabIndex = 14;
            // 
            // btnCopy
            // 
            this.btnCopy.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnCopy.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCopy.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCopy.Image = ((System.Drawing.Image)(resources.GetObject("btnCopy.Image")));
            this.btnCopy.Location = new System.Drawing.Point(608, 35);
            this.btnCopy.Name = "btnCopy";
            this.btnCopy.Size = new System.Drawing.Size(31, 28);
            this.btnCopy.TabIndex = 15;
            this.btnCopy.UseVisualStyleBackColor = false;
            this.btnCopy.Click += new System.EventHandler(this.btnCopy_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRefresh.Image = global::Medienmanager.Properties.Resources.refresh;
            this.btnRefresh.Location = new System.Drawing.Point(675, 35);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(31, 28);
            this.btnRefresh.TabIndex = 16;
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnZoomout
            // 
            this.btnZoomout.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnZoomout.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnZoomout.Image = ((System.Drawing.Image)(resources.GetObject("btnZoomout.Image")));
            this.btnZoomout.Location = new System.Drawing.Point(744, 35);
            this.btnZoomout.Name = "btnZoomout";
            this.btnZoomout.Size = new System.Drawing.Size(31, 28);
            this.btnZoomout.TabIndex = 17;
            this.btnZoomout.UseVisualStyleBackColor = false;
            this.btnZoomout.Click += new System.EventHandler(this.btnZoom_Click);
            // 
            // btnRename
            // 
            this.btnRename.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnRename.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnRename.Image = ((System.Drawing.Image)(resources.GetObject("btnRename.Image")));
            this.btnRename.Location = new System.Drawing.Point(884, 35);
            this.btnRename.Name = "btnRename";
            this.btnRename.Size = new System.Drawing.Size(31, 28);
            this.btnRename.TabIndex = 20;
            this.btnRename.UseVisualStyleBackColor = false;
            this.btnRename.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.txtSearchFile);
            this.panel1.Location = new System.Drawing.Point(6, 35);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(191, 28);
            this.panel1.TabIndex = 22;
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(158, 0);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(31, 28);
            this.btnSearch.TabIndex = 23;
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtSearchFile
            // 
            this.txtSearchFile.Location = new System.Drawing.Point(0, 1);
            this.txtSearchFile.Multiline = true;
            this.txtSearchFile.Name = "txtSearchFile";
            this.txtSearchFile.Size = new System.Drawing.Size(160, 24);
            this.txtSearchFile.TabIndex = 23;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblPath);
            this.panel2.Location = new System.Drawing.Point(233, 73);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(682, 25);
            this.panel2.TabIndex = 23;
            // 
            // pnlPathImg
            // 
            this.pnlPathImg.BackColor = System.Drawing.SystemColors.Highlight;
            this.pnlPathImg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPathImg.Controls.Add(this.lblPathImg);
            this.pnlPathImg.Location = new System.Drawing.Point(203, 73);
            this.pnlPathImg.Name = "pnlPathImg";
            this.pnlPathImg.Size = new System.Drawing.Size(29, 25);
            this.pnlPathImg.TabIndex = 24;
            // 
            // lblPathImg
            // 
            this.lblPathImg.AutoSize = true;
            this.lblPathImg.BackColor = System.Drawing.SystemColors.Highlight;
            this.lblPathImg.Image = ((System.Drawing.Image)(resources.GetObject("lblPathImg.Image")));
            this.lblPathImg.Location = new System.Drawing.Point(2, 5);
            this.lblPathImg.Name = "lblPathImg";
            this.lblPathImg.Size = new System.Drawing.Size(22, 13);
            this.lblPathImg.TabIndex = 25;
            this.lblPathImg.Text = "     ";
            // 
            // btnZoomin
            // 
            this.btnZoomin.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnZoomin.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnZoomin.Image = ((System.Drawing.Image)(resources.GetObject("btnZoomin.Image")));
            this.btnZoomin.Location = new System.Drawing.Point(817, 36);
            this.btnZoomin.Name = "btnZoomin";
            this.btnZoomin.Size = new System.Drawing.Size(31, 28);
            this.btnZoomin.TabIndex = 25;
            this.btnZoomin.UseVisualStyleBackColor = false;
            this.btnZoomin.Click += new System.EventHandler(this.btnZoomin_Click);
            // 
            // lblDocPreview
            // 
            this.lblDocPreview.AutoSize = true;
            this.lblDocPreview.Location = new System.Drawing.Point(1060, 50);
            this.lblDocPreview.Name = "lblDocPreview";
            this.lblDocPreview.Size = new System.Drawing.Size(97, 13);
            this.lblDocPreview.TabIndex = 26;
            this.lblDocPreview.Text = "Document Preview";
            // 
            // mediaManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1296, 698);
            this.Controls.Add(this.lblDocPreview);
            this.Controls.Add(this.btnZoomin);
            this.Controls.Add(this.pnlPathImg);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnRename);
            this.Controls.Add(this.btnZoomout);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnCopy);
            this.Controls.Add(this.btnRight);
            this.Controls.Add(this.btnLeft);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.lsvFiles);
            this.Controls.Add(this.btnMove);
            this.Controls.Add(this.btnAddFolder);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.tvwMediaManager);
            this.Controls.Add(this.pnlPreview);
            this.Controls.Add(this.menuStrip2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "mediaManager";
            this.Text = "Media Manager";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnlPathImg.ResumeLayout(false);
            this.pnlPathImg.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlPreview;
        private System.Windows.Forms.ToolStripMenuItem dateiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem öffnenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem schließenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bearbeitenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kopierenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem löschenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annotierenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem medientypToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem textDokumenteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem audioFilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem videoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bilderToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ImageList ilstTreeView;
        private System.Windows.Forms.TreeView tvwMediaManager;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.Button btnAddFolder;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnMove;
        private System.Windows.Forms.ImageList ilstFileAndIcon;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Label lblPath;
        private System.Windows.Forms.Button btnCopy;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ListView lsvFiles;
        private System.Windows.Forms.Button btnZoomout;
        private System.Windows.Forms.Button btnRename;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem andereToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtSearchFile;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlPathImg;
        private System.Windows.Forms.Label lblPathImg;
        private System.Windows.Forms.Button btnZoomin;
        private System.Windows.Forms.Label lblDocPreview;
    }
}

