﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Medienmanager
{
    public partial class frmMoveFiles : Form
    {
        string currentPath = "";
        List<string> selectedItems;
        private mediaManager parentForm = null;
        public frmMoveFiles()
        {
            InitializeComponent();
        }
        public frmMoveFiles(Form callingForm, string path, List<string> items)
        {
           parentForm = callingForm as mediaManager;
           currentPath = path;
           selectedItems = items;
           //MessageBox.Show(item);
           InitializeComponent();
        }

        private void cboPath_Click(object sender, EventArgs e)
        {
            string[] folders = currentPath.Split('\\');
            //MessageBox.Show(currentPath);
            string newPath = "";
            cboPath.Items.Clear();
            for (int j = 0; j < folders.Length - 1; j++)
            {
                newPath += folders[j] + "\\";
                cboPath.Items.Add(newPath);
            }
            DirectoryInfo dirInfo = new DirectoryInfo(currentPath);

            if (dirInfo.GetDirectories().Length > 0)
            {
                foreach (DirectoryInfo dir in dirInfo.GetDirectories())
                {
                    cboPath.Items.Add(dir.FullName);
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            parentForm.moveFiles(currentPath, cboPath.SelectedItem.ToString(), selectedItems);
            this.Close();
        }

        private void btnAbbrechen_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
