﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Medienmanager
{
    public partial class frmNewFolder : Form
    {
        string folderName = "";
        private mediaManager parentForm = null;

        public frmNewFolder()
        {
            InitializeComponent();
        }
        public frmNewFolder(Form callingForm)
        {
           parentForm = callingForm as mediaManager;
           InitializeComponent();
        }

        private void btnAbbrechen_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            folderName = txtFolderName.Text;
            if (folderName == "")
                MessageBox.Show("Geben Sie Ordner Name ein!");
            else
            {
                parentForm.createDir(folderName);
                this.Close();
            }   
        }
    }
}
