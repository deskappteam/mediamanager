﻿namespace Medienmanager
{
    partial class copyFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.btnAbbrechen = new System.Windows.Forms.Button();
            this.cboPath = new System.Windows.Forms.ComboBox();
            this.lblChoosePath = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(534, 74);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 7;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnAbbrechen
            // 
            this.btnAbbrechen.Location = new System.Drawing.Point(12, 74);
            this.btnAbbrechen.Name = "btnAbbrechen";
            this.btnAbbrechen.Size = new System.Drawing.Size(75, 23);
            this.btnAbbrechen.TabIndex = 6;
            this.btnAbbrechen.Text = "Abbrechen";
            this.btnAbbrechen.UseVisualStyleBackColor = true;
            this.btnAbbrechen.Click += new System.EventHandler(this.btnAbbrechen_Click);
            // 
            // cboPath
            // 
            this.cboPath.FormattingEnabled = true;
            this.cboPath.Location = new System.Drawing.Point(12, 33);
            this.cboPath.Name = "cboPath";
            this.cboPath.Size = new System.Drawing.Size(597, 21);
            this.cboPath.TabIndex = 5;
            this.cboPath.Text = "Wählen Sie  einen Pfad ein";
            this.cboPath.Click += new System.EventHandler(this.cboPath_Click);
            // 
            // lblChoosePath
            // 
            this.lblChoosePath.AutoSize = true;
            this.lblChoosePath.Location = new System.Drawing.Point(12, 9);
            this.lblChoosePath.Name = "lblChoosePath";
            this.lblChoosePath.Size = new System.Drawing.Size(128, 13);
            this.lblChoosePath.TabIndex = 4;
            this.lblChoosePath.Text = "Wählen Sie den Pfad ein:";
            // 
            // copyFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 105);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnAbbrechen);
            this.Controls.Add(this.cboPath);
            this.Controls.Add(this.lblChoosePath);
            this.Name = "copyFile";
            this.Text = "Copy File";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnAbbrechen;
        private System.Windows.Forms.ComboBox cboPath;
        private System.Windows.Forms.Label lblChoosePath;
    }
}