﻿namespace Medienmanager
{
    partial class renameFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOldname = new System.Windows.Forms.Label();
            this.txtOldname = new System.Windows.Forms.TextBox();
            this.txtNewname = new System.Windows.Forms.TextBox();
            this.lblNewname = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblOldname
            // 
            this.lblOldname.AutoSize = true;
            this.lblOldname.Location = new System.Drawing.Point(5, 11);
            this.lblOldname.Name = "lblOldname";
            this.lblOldname.Size = new System.Drawing.Size(54, 13);
            this.lblOldname.TabIndex = 0;
            this.lblOldname.Text = "Old Name";
            // 
            // txtOldname
            // 
            this.txtOldname.Location = new System.Drawing.Point(75, 8);
            this.txtOldname.Name = "txtOldname";
            this.txtOldname.ReadOnly = true;
            this.txtOldname.Size = new System.Drawing.Size(197, 20);
            this.txtOldname.TabIndex = 1;
            // 
            // txtNewname
            // 
            this.txtNewname.Location = new System.Drawing.Point(75, 46);
            this.txtNewname.Name = "txtNewname";
            this.txtNewname.Size = new System.Drawing.Size(197, 20);
            this.txtNewname.TabIndex = 3;
            // 
            // lblNewname
            // 
            this.lblNewname.AutoSize = true;
            this.lblNewname.Location = new System.Drawing.Point(5, 49);
            this.lblNewname.Name = "lblNewname";
            this.lblNewname.Size = new System.Drawing.Size(60, 13);
            this.lblNewname.TabIndex = 2;
            this.lblNewname.Text = "New Name";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(75, 77);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(70, 22);
            this.btnOk.TabIndex = 4;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(202, 77);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(70, 22);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // renameFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(279, 106);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.txtNewname);
            this.Controls.Add(this.lblNewname);
            this.Controls.Add(this.txtOldname);
            this.Controls.Add(this.lblOldname);
            this.Name = "renameFile";
            this.Text = "Rename File";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblOldname;
        private System.Windows.Forms.TextBox txtOldname;
        private System.Windows.Forms.TextBox txtNewname;
        private System.Windows.Forms.Label lblNewname;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
    }
}