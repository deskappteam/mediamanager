﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using AxWMPLib;
using AxAcroPDFLib;
using System.Text.RegularExpressions;
using System.Diagnostics;


namespace Medienmanager
{
    public partial class mediaManager : Form
    {
        DirectoryInfo directoryInfo;
        string currentPath = "";
        string oldPath = "";
        List<Image> imageList = new List<Image>();
        int search = 0;
        public mediaManager()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tvwMediaManager.ExpandAll();      

            tvwMediaManager.ImageList = ilstTreeView;
            lsvFiles.LargeImageList = ilstFileAndIcon;
            // toolpip für control botton
            ToolTip toolTip1 = new ToolTip();

            toolTip1.ShowAlways = true;
            toolTip1.SetToolTip(btnLeft, "Rückwärst");
            toolTip1.SetToolTip(btnUpload, "Upload");
            toolTip1.SetToolTip(btnRight, "Vorwärst");
            toolTip1.SetToolTip(btnAddFolder, "neue Ordner");
            toolTip1.SetToolTip(btnDelete, "löschen");
            toolTip1.SetToolTip(btnMove, "move");
            toolTip1.SetToolTip(btnCopy, "kopieren");
            toolTip1.SetToolTip(btnRefresh, "aktualisieren");
            toolTip1.SetToolTip(btnZoomout, "zoom -");
            toolTip1.SetToolTip(btnZoomin, "zoom +");
            toolTip1.SetToolTip(btnRename, "Anotieren");
            //  control botton eanable
            btnLeft.Enabled = false;
            btnUpload.Enabled = true;
            btnRight.Enabled = false;
            btnAddFolder.Enabled = false;
            btnDelete.Enabled = false;
            btnMove.Enabled = false;
            btnCopy.Enabled = false;
            btnRefresh.Enabled = false;
            btnZoomout.Enabled = false;
            btnZoomin.Enabled = false;
            btnRename.Enabled = false;
            btnSearch.Enabled = false;



        }

        // filtern für alle Medientyp
        private void tvwMediaManager_AfterSelect(object sender, TreeViewEventArgs e)

        {
            if (tvwMediaManager.SelectedNode.Name == "mediaManager" && tvwMediaManager.SelectedNode.IsSelected == true)
            {
                tvwMediaManager.SelectedNode.SelectedImageIndex = 0;
                if (currentPath == "")
                {
                    MessageBox.Show("Laden Sie erst die Datei hoch!","Info");
                }
                else
                {
                    showDirContents(currentPath);
                }
            }            
            else if (tvwMediaManager.SelectedNode.Name == "nodeText" && tvwMediaManager.SelectedNode.IsSelected == true)
            {
                tvwMediaManager.SelectedNode.SelectedImageIndex = 2;
                if (currentPath == "")
                {
                    MessageBox.Show("Laden Sie erst die Datei hoch!","Info");
                }
                else
                {
                    filterFiles("text", lsvFiles);
                }
            }
            else if(tvwMediaManager.SelectedNode.Name == "nodeAudio" && tvwMediaManager.SelectedNode.IsSelected == true)
            {
                tvwMediaManager.SelectedNode.SelectedImageIndex = 1;
                if (currentPath == "")
                {
                    MessageBox.Show("Laden Sie erst die Datei hoch!", "Info");
                }
                else
                {
                    filterFiles("audio", lsvFiles);
                }
            }
            else if (tvwMediaManager.SelectedNode.Name == "nodeVideo" && tvwMediaManager.SelectedNode.IsSelected == true)
            {
                tvwMediaManager.SelectedNode.SelectedImageIndex = 5;

                if (currentPath == "")
                {
                    MessageBox.Show("Laden Sie erst die Datei hoch!","Info");
                }
                else
                {
                    filterFiles("video", lsvFiles);
                }
            }
            else if (tvwMediaManager.SelectedNode.Name == "nodePicture" && tvwMediaManager.SelectedNode.IsSelected == true)
            {
                tvwMediaManager.SelectedNode.SelectedImageIndex = 4;

                if (currentPath == "")
                {
                    MessageBox.Show("Laden Sie erst die Datei hoch!","Info");
                }
                else
                {
                    filterFiles("picture", lsvFiles);
                }
            }
            else if (tvwMediaManager.SelectedNode.Name == "nodeMix" && tvwMediaManager.SelectedNode.IsSelected == true)
            {
                if (currentPath == "")
                {
                    MessageBox.Show("Laden Sie erst die Datei hoch!","Info");
                }
                else
                {
                    filterFiles("mix", lsvFiles);
                }
            }
        }

        // Filtern die Datei nach Datei Typ
        private void filterFiles(string fileType, ListView lsvFiles)
        {
            directoryInfo = new DirectoryInfo(currentPath);
            lsvFiles.Items.Clear();
            ListViewItem item;
            lsvFiles.BeginUpdate();
            if (fileType.Equals("text"))
            {
                foreach (FileInfo fileObj in directoryInfo.GetFiles())
                {
                    if ((fileObj.FullName).EndsWith(".txt") || (fileObj.FullName).EndsWith(".doc") || (fileObj.FullName).EndsWith(".pdf") || (fileObj.FullName).EndsWith(".docx") || (fileObj.FullName).EndsWith(".odt") || (fileObj.FullName).EndsWith(".ppt") || (fileObj.FullName).EndsWith(".pptx") || (fileObj.FullName).EndsWith(".rtf"))
                    {
                        item = getFileWithImageExt(fileObj);
                        lsvFiles.Items.Add(item);
                    }
                }
            }
            else if (fileType.Equals("audio"))
            {
                foreach (FileInfo fileObj in directoryInfo.GetFiles())
                {
                    if ((fileObj.FullName).EndsWith(".mp3") || (fileObj.FullName).EndsWith(".cda"))
                    {
                        item = getFileWithImageExt(fileObj);
                        lsvFiles.Items.Add(item);
                    }
                }
            }
            else if (fileType.Equals("video"))
            {
                foreach (FileInfo fileObj in directoryInfo.GetFiles())
                {
                    if ((fileObj.FullName).EndsWith(".wmv") || (fileObj.FullName).EndsWith(".mpeg") || (fileObj.FullName).EndsWith(".mp4") || (fileObj.FullName).EndsWith(".flv") || (fileObj.FullName).EndsWith(".mkv") || (fileObj.FullName).EndsWith(".mov"))
                    {
                        item = getFileWithImageExt(fileObj);
                        lsvFiles.Items.Add(item);
                    }
                }
            }
            else if (fileType.Equals("picture"))
            {
                foreach (FileInfo fileObj in directoryInfo.GetFiles())
                {
                    if ((fileObj.FullName).EndsWith(".jpg") || (fileObj.FullName).EndsWith(".png") || (fileObj.FullName).EndsWith(".svg") || (fileObj.FullName).EndsWith(".gif") || (fileObj.FullName).EndsWith(".ico") || (fileObj.FullName).EndsWith(".bmp") || (fileObj.FullName).EndsWith(".eps") || (fileObj.FullName).EndsWith(".tiff"))
                    {
                        item = getFileWithImageExt(fileObj);
                        lsvFiles.Items.Add(item);
                    }
                }
            }
            else if (fileType.Equals("mix")) 
            {
                foreach (FileInfo fileObj in directoryInfo.GetFiles())
                {
                    if (!(fileObj.FullName).EndsWith(".jpg") && !(fileObj.FullName).EndsWith(".png") && !(fileObj.FullName).EndsWith(".svg") && !(fileObj.FullName).EndsWith(".gif") && !(fileObj.FullName).EndsWith(".ico") && !(fileObj.FullName).EndsWith(".bmp") && !(fileObj.FullName).EndsWith(".eps") && !(fileObj.FullName).EndsWith(".tiff")
                        && !(fileObj.FullName).EndsWith(".wmv") && !(fileObj.FullName).EndsWith(".mpeg") && !(fileObj.FullName).EndsWith(".mp4") && !(fileObj.FullName).EndsWith(".flv") && !(fileObj.FullName).EndsWith(".mkv") && !(fileObj.FullName).EndsWith(".mov")
                        && !(fileObj.FullName).EndsWith(".mp3") && !(fileObj.FullName).EndsWith(".cda")
                        && !(fileObj.FullName).EndsWith(".txt") && !(fileObj.FullName).EndsWith(".doc") && !(fileObj.FullName).EndsWith(".pdf") && !(fileObj.FullName).EndsWith(".docx") && !(fileObj.FullName).EndsWith(".odt") && !(fileObj.FullName).EndsWith(".ppt") && !(fileObj.FullName).EndsWith(".pptx") && !(fileObj.FullName).EndsWith(".rtf"))
                    {
                        item = getFileWithImageExt(fileObj);
                        lsvFiles.Items.Add(item);
                    }
                }
            }

            
            lsvFiles.EndUpdate();
        }


        //filesendung nach icon einordnen 
        private ListViewItem getFileWithImageExt(FileInfo fileObj)
        {
            ListViewItem item;
            //https://msdn.microsoft.com/de-de/library/ms404308(v=vs.110).aspx
            // Set a default icon for the file.
            Icon iconForFile = SystemIcons.WinLogo;

            item = new ListViewItem(fileObj.Name, 1);
            iconForFile = Icon.ExtractAssociatedIcon(fileObj.FullName);

            // Check to see if the image collection contains an image
            // for this extension, using the extension as a key.
            if (!ilstFileAndIcon.Images.ContainsKey(fileObj.Extension))
            {
                // If not, add the image to the image list.
                iconForFile = System.Drawing.Icon.ExtractAssociatedIcon(fileObj.FullName);
                ilstFileAndIcon.Images.Add(fileObj.Extension, iconForFile);
            }
            item.ImageKey = fileObj.Extension;

            return item;
        }

        

        private void showDirContents(string path)
        {
            directoryInfo = new DirectoryInfo(path);
            
            lsvFiles.Items.Clear();
            ListViewItem item;

            lsvFiles.BeginUpdate();
            //Exception from system
            try {
                foreach (DirectoryInfo dir in directoryInfo.GetDirectories())
                {
                    item = new ListViewItem(dir.Name, 0);
                    lsvFiles.Items.Add(item);
                }

                foreach (FileInfo file in directoryInfo.GetFiles())
                {
                    item = getFileWithImageExt(file);                    
                    lsvFiles.Items.Add(item);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message,"Fehler Meldung");
            }

            lsvFiles.EndUpdate();
        }
        // control button function
        private void btnUpload_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
 
            if(fbd.ShowDialog() == DialogResult.OK)
            {
                //MessageBox.Show(fbd.SelectedPath);
                showDirContents(fbd.SelectedPath);
                currentPath = fbd.SelectedPath;
                oldPath = currentPath;
            }
            else { }
                       
            oldPath = currentPath;         
            
            lblPath.Text = currentPath;

            lsvFiles.ShowGroups = false;
            //control button wieder aktualisieren
            btnLeft.Enabled = true;
            btnUpload.Enabled = true;
            btnRight.Enabled = true;
            btnAddFolder.Enabled = true;
            btnDelete.Enabled = true;
            btnMove.Enabled = true;
            btnCopy.Enabled = true;
            btnRefresh.Enabled = true;
            btnZoomout.Enabled = true;
            btnZoomin.Enabled = true;
            btnRename.Enabled = true;
            btnSearch.Enabled = true;
        }

        private void btnAddFolder_Click(object sender, EventArgs e)
        {
            frmNewFolder newFolderForm = new frmNewFolder(this);
            newFolderForm.Show();
        }

        private void schließenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //New folder added
        public void createDir(string folderName)
        {
            string path = "";

            if(currentPath.Equals("C:\\"))
            {
                path += currentPath + folderName;
            }
            else
            {
                path += currentPath + "\\" + folderName;
            }

            try
            {
                // Determine whether the directory exists.
                if (Directory.Exists(path))
                {
                    // Try to create the directory.
                    MessageBox.Show("Ordner existiert schon!");
                    return;
                }
                DirectoryInfo di = Directory.CreateDirectory(path);
                showDirContents(currentPath);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Fehler Meldung");
            } 
        }        
        // delete
        private void btnDelete_Click(object sender, EventArgs e)
        {
            string path = "";
            
            try
            {
                if (lsvFiles.SelectedItems.Count > 1) // falls mehrere files oder Odner selectieren
                {
                    DialogResult result = MessageBox.Show("Wollen wirklich die Dateien löschen?", "Bestätigung", MessageBoxButtons.OKCancel);

                    if (result == DialogResult.OK)
                    {
                        foreach (ListViewItem item in lsvFiles.SelectedItems)
                        {
                            path = currentPath + "\\" + item.Text;

                            if (isFolder(path))
                            {
                                Directory.Delete(path, true); // ordner wird gelöscht
                            }
                            else
                            {
                                File.Delete(path); //files wird gelöscht
                            }
                        }
                    }
                    else { }
                    showDirContents(currentPath);
                }
                else // falls ein file oder Odner selectieren 
                {
                    string selectedItem = lsvFiles.SelectedItems[0].Text;
                    path = currentPath + "\\" + selectedItem;

                    if (isFolder(path))
                    {
                        DialogResult result = MessageBox.Show("Wollen wirklich den Ordner löschen?", "Bestätigung", MessageBoxButtons.OKCancel);

                        if (result == DialogResult.OK)
                        {
                            Directory.Delete(path, true);
                            showDirContents(currentPath);
                        }
                        else { }
                    }
                    else
                    {
                        DialogResult result = MessageBox.Show("Wollen wirklich diese Datei löschen?", "Bestätigung", MessageBoxButtons.OKCancel);

                        if (result == DialogResult.OK)
                        {
                            File.Delete(path);
                            showDirContents(currentPath);
                        }
                        else { }
                    }             
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            } 
        }
        //form für move
        private void btnMove_Click(object sender, EventArgs e)
        {
             if (lsvFiles.SelectedItems.Count == 0)
            {
                MessageBox.Show("Selektieren Sie zuerst eine Datei!","Info");
            }
             else
             {
                 var selectedItems = new List<string>();
                 //string item = listView1.SelectedItems[0].Text;
                 foreach (ListViewItem i in lsvFiles.SelectedItems)
                 {
                     selectedItems.Add(i.Text.ToString());
                 }
                 //ListView[] selectedItems = listView1.SelectedItems;
                 frmMoveFiles moveFilesForm = new frmMoveFiles(this, currentPath, selectedItems);
                 moveFilesForm.Show();
             }            
        }

        //funktion für move
        public void moveFiles(string srcPath, string destPath, List<string> items)
        {
            //FileAttributes attr = File.GetAttributes(srcPath + "\\" + selectedItem);

            foreach(string item in items)
            {
                string path = srcPath + "\\" + item;

                //detect whether its a directory or file
                if (isFolder(path))
                {

                    if (Directory.Exists(destPath + "\\" + item))
                    {
                        return;
                    }
                    else
                    {
                        DirectoryInfo di = Directory.CreateDirectory(destPath + "\\" + item);
                    }

                    try
                    {

                        //http://stackoverflow.com/questions/58744/best-way-to-copy-the-entire-contents-of-a-directory-in-c-sharp

                        //Now Create all of the directories
                        foreach (string dirPath in Directory.GetDirectories(path, "*",
                            SearchOption.AllDirectories))
                            Directory.CreateDirectory(dirPath.Replace(path, destPath + "\\" + item));

                        //Copy all the files & Replaces any files with the same name
                        foreach (string newPath in Directory.GetFiles(path, "*.*",
                            SearchOption.AllDirectories))
                            File.Copy(newPath, newPath.Replace(path, destPath + "\\" + item), true);



                        Directory.Delete(path, true);

                        showDirContents(srcPath);


                        //MessageBox.Show("PATH:" + path + "\n" + "DEST:" + destPath);
                        ////https://msdn.microsoft.com/de-de/library/system.io.directory.move(v=vs.110).aspx
                        //Directory.Move(path, destPath);
                        ////MessageBox.Show("Its a directory");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message,"Fehler Meldung");
                    }
                }
                else
                {
                    //MessageBox.Show("Its a file");
                    if (File.Exists(destPath + "\\" + item)) {
                        File.Delete(destPath + "\\" + item);
                    }

                    File.Move(srcPath + "\\" + item, destPath + "\\" + item);
                    File.Delete(srcPath + "\\" + item);
                    
                }       
            }
            showDirContents(srcPath);      
        }
        //buton left( zurück)
        private void btnFolderUp_Click(object sender, EventArgs e)
        {
            removeCtrl();
            if (currentPath == "")
            {
                MessageBox.Show("Laden Sie erst die Datei hoch!");
            }
            else
            {
                if (!isDrive(currentPath))
                {
                    oldPath = currentPath;
                    string[] folders = currentPath.Split('\\');
                    string newPath = "";

                    for (int j = 0; j < folders.Length - 1; j++)
                    {
                        newPath = (j == folders.Length - 2) ? newPath += folders[j] : newPath += folders[j] + "\\";
                    }
                    //MessageBox.Show("Current Path: " + currentPath + "\nNew Path:" + newPath);
                    if (newPath.Length == 2 && newPath[1] == ':')
                        newPath += "\\";
                    showDirContents(newPath);
                    currentPath = newPath;
                }
                else
                {
                    btnLeft.Enabled = false;
                }
                lblPath.Text = currentPath;

            }
        }
        // überprüfen ob es ein C drive ist.
        private bool isDrive(string path)
        {
            DriveInfo[] allDrives = DriveInfo.GetDrives();

            foreach (DriveInfo drive in allDrives)
            {
                if(path.Equals(drive.ToString()))
                {
                    return true;
                }
            }
            return false;            
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            string item;

            if(search == 0)
            {
                item = lsvFiles.SelectedItems[0].Text;

                string path = (currentPath + "\\" + item);

                if (isFolder(path))
                {
                    currentPath = (isDrive(currentPath)) ? currentPath += item : currentPath += "\\" + item;

                    //MessageBox.Show(currentPath);

                    showDirContents(currentPath);
                    btnLeft.Enabled = true;
                    oldPath = currentPath;
                }
                else
                {
                    System.Diagnostics.Process.Start(path);
                }
                
            }
            else if (search == 1)
            {
                item = lsvFiles.SelectedItems[0].Text;
                System.Diagnostics.Process.Start(item);
            }
            lblPath.Text = currentPath;
        }

        private bool isFolder(string path)
        {
            FileAttributes attr = File.GetAttributes(path);

            if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
            {
                return true;
            }
            else
            {
                return false;
            }  
        }
        // botton Right (vorwärst)
        private void button1_Click(object sender, EventArgs e)
        {
            removeCtrl();
            if (Directory.Exists(oldPath))
            {
                if (lsvFiles.SelectedItems.Count > 0)
                {
                    string path = (currentPath + "\\" + lsvFiles.SelectedItems[0].Text);
                    //MessageBox.Show("NEW");
                    if (isFolder(path))
                    {
                        showDirContents(path);
                        currentPath = path;
                        oldPath = currentPath;
                        lblPath.Text = currentPath;
                    }
                }
                else
                {
                    if (!oldPath.Equals(currentPath))
                    {
                        //MessageBox.Show("OlD");
                        showDirContents(oldPath);
                        currentPath = oldPath;
                        lblPath.Text = currentPath;
                    }
                    else
                    {
                        MessageBox.Show("Wählen Sie einen Ordner aus!","Info");
                    }
                }                
            }
            else
            {
                MessageBox.Show("Pfad existiert nicht","Info");
            }
        }
        //form für rename
        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (lsvFiles.SelectedItems.Count == 0)
            {
                MessageBox.Show("Selektieren Sie zuerst eine Datei!","Info");
            }
            else 
            {
                string item = lsvFiles.SelectedItems[0].Text;
                renameFile renamefileObj = new renameFile(this, currentPath, item);
                renamefileObj.Show();
            }            
        }
        // funktion rename
        public void renamefiles(string srcPath, string destPath)
        {
            try
            {
                File.Move(srcPath, destPath);
                showDirContents(currentPath);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }                      
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            txtSearchFile.Text = "";
            lsvFiles.View = View.Tile;
            lsvFiles.Enabled = true;
            search = 0;
            if(currentPath == "")
            {
                MessageBox.Show("Laden sie die Ordner hoch","Info");
            }
            else
            {
                showDirContents(currentPath);
            }
        }
        //form copy
        private void btnCopy_Click(object sender, EventArgs e)
        {
            if (lsvFiles.SelectedItems.Count == 0)
            {
                MessageBox.Show("Selektieren Sie zuerst eine Datei!","Info");
            }
             else
             {
                 var selectedItems = new List<string>();
                 //string item = listView1.SelectedItems[0].Text;
                 foreach (ListViewItem i in lsvFiles.SelectedItems)
                 {
                     selectedItems.Add(i.Text.ToString());
                 }
                 //ListView[] selectedItems = listView1.SelectedItems;
                 copyFile copyFileObj = new copyFile(this, currentPath, selectedItems);
                 copyFileObj.Show();
             }
        }

        // funktion copy
        public void copyFiles(string srcPath, string destPath, List<string> items)
        {
            //FileAttributes attr = File.GetAttributes(srcPath + "\\" + selectedItem);

            foreach (string item in items)
            {
                string path = srcPath + "\\" + item;

                //detect whether its a directory or file
                if (isFolder(path))
                {

                    if (Directory.Exists(destPath + "\\" + item))
                    {
                        return;
                    }
                    else
                    {
                        DirectoryInfo di = Directory.CreateDirectory(destPath + "\\" + item);
                    }

                    try
                    {

                        //http://stackoverflow.com/questions/58744/best-way-to-copy-the-entire-contents-of-a-directory-in-c-sharp

                        //Now Create all of the directories
                        foreach (string dirPath in Directory.GetDirectories(path, "*",
                            SearchOption.AllDirectories))
                            Directory.CreateDirectory(dirPath.Replace(path, destPath + "\\" + item));

                        //Copy all the files & Replaces any files with the same name
                        foreach (string newPath in Directory.GetFiles(path, "*.*",
                            SearchOption.AllDirectories))
                            File.Copy(newPath, newPath.Replace(path, destPath + "\\" + item), true);

                        showDirContents(srcPath);


                        //MessageBox.Show("PATH:" + path + "\n" + "DEST:" + destPath);
                        ////https://msdn.microsoft.com/de-de/library/system.io.directory.move(v=vs.110).aspx
                        //Directory.Move(path, destPath);
                        ////MessageBox.Show("Its a directory");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message,"Fehler Meldung");
                    }
                }
                else
                {
                    //MessageBox.Show("Its a file");
                    File.Copy(srcPath + "\\" + item, destPath + "\\" + item,true);
                }
            }
            showDirContents(srcPath);
        }

        private void btnZoom_Click(object sender, EventArgs e)
        {
            SetImageSize('-');
        }

        private void SetImageSize(char zoom)
        { 
            if (imageList.Count == 0)
            {
                foreach (Image i in ilstFileAndIcon.Images)
                {
                    imageList.Add(i);
                }
            }
            if(zoom == '+')
            {
                ilstFileAndIcon.ImageSize = new Size(ilstFileAndIcon.ImageSize.Width + 5, ilstFileAndIcon.ImageSize.Height + 5);
            }
            else if(zoom == '-')
            {
                if (ilstFileAndIcon.ImageSize.Width > 5)
                {
                    ilstFileAndIcon.ImageSize = new Size(ilstFileAndIcon.ImageSize.Width - 5, ilstFileAndIcon.ImageSize.Height - 5);
                }
                else
                {
                    ilstFileAndIcon.ImageSize = new Size(5,5);
                }
            }

            ilstFileAndIcon.ColorDepth = ColorDepth.Depth32Bit;
            ilstFileAndIcon.TransparentColor = System.Drawing.Color.Transparent;
            foreach (Image i in imageList)
            {
                ilstFileAndIcon.Images.Add(i);
            }
            int fontsize = 0;
            if (ilstFileAndIcon.ImageSize.Width <= 32)
            {
                fontsize = 8;
            }
            else if (ilstFileAndIcon.ImageSize.Width <= 64)
            {
                fontsize = 12;
            }
            else
            {
                fontsize = 16;
            }
            Font f = new Font(lsvFiles.Font.FontFamily, fontsize);
            lsvFiles.Font = f;
        }

        private void btnZoomin_Click(object sender, EventArgs e)
        {
            SetImageSize('+');
        }

        private void lsvFiles_Click(object sender, EventArgs e)
        {
            string item = "";
            if(search == 0)
            {
                item = lsvFiles.SelectedItems[0].Text;
                if (isFolder(currentPath + "\\" + item))
                {
                    if (pnlPreview.Controls.Count > 0)
                    {
                        removeCtrl();
                    }
                    PictureBox picBoxObj = new PictureBox();
                    picBoxObj.Location = new Point(185, 310);
                    picBoxObj.Name = "ImageBox";
                    picBoxObj.SizeMode = PictureBoxSizeMode.Zoom;
                    picBoxObj.Image = ilstFileAndIcon.Images[0];
                    pnlPreview.Controls.Add(picBoxObj);
                }
                else
                {
                    if (pnlPreview.Controls.Count > 0)
                    {
                        removeCtrl();
                    }
                    showFilePreview(currentPath + "\\" + item);
                }
            }
            else if(search == 1)
            {
                item = lsvFiles.SelectedItems[0].Text;
                if (pnlPreview.Controls.Count > 0)
                {
                    removeCtrl();
                }
                showFilePreview(item);
            }           
        }
        //preeview löscht die ctrl von prewview
        private void removeCtrl()
        {
            foreach (Control pnlCtrl in pnlPreview.Controls)
            {
                // Determine whether the control is textBox1,
                // and if it is, remove it.
                if (pnlCtrl.Name == "Mediaplayer" || pnlCtrl.Name == "PDFReader")
                {
                    pnlCtrl.Dispose();
                    pnlPreview.Controls.Remove(pnlCtrl);
                }
                else
                {
                    pnlPreview.Controls.Remove(pnlCtrl);
                }
            }
        }
        //Preeview zeigen
        private void showFilePreview(string filePath)
        {
            try 
            {

                if (Path.GetExtension(filePath).Equals(".jpg") || Path.GetExtension(filePath).Equals(".jpeg") || Path.GetExtension(filePath).Equals(".png") || Path.GetExtension(filePath).Equals(".gif") || Path.GetExtension(filePath).Equals(".bmp"))
             {
                 PictureBox picBoxObj = new PictureBox();
                 picBoxObj.Height = pnlPreview.Height;
                 picBoxObj.Width = pnlPreview.Width;
                 picBoxObj.Name = "ImageBox";
                 picBoxObj.SizeMode = PictureBoxSizeMode.Zoom;
                 //picBoxObj.Image = Image.FromFile(filePath);
                 picBoxObj.Image = new Bitmap(filePath);
                 pnlPreview.Controls.Add(picBoxObj);
             }
                else if (Path.GetExtension(filePath).Equals(".txt"))
             {
                 RichTextBox richTextBoxObj = new RichTextBox();
                 richTextBoxObj.Height = pnlPreview.Height;
                 richTextBoxObj.Width = pnlPreview.Width;
                 richTextBoxObj.Name = "RichText";
                 string content = File.ReadAllText(filePath);
                 richTextBoxObj.Text = content;
                 richTextBoxObj.ReadOnly = true;
                 richTextBoxObj.BackColor = this.BackColor;
                 richTextBoxObj.BorderStyle = BorderStyle.None;
                 pnlPreview.Controls.Add(richTextBoxObj);

             }
                else if (Path.GetExtension(filePath).Equals(".mp3") || Path.GetExtension(filePath).Equals(".mp4") || Path.GetExtension(filePath).Equals(".wav"))
            {
                AxWindowsMediaPlayer mediaPlayer= new AxWindowsMediaPlayer();

                mediaPlayer.Height = pnlPreview.Height;
                mediaPlayer.Width = pnlPreview.Width;
                mediaPlayer.Name = "Mediaplayer";         

                pnlPreview.Controls.Add(mediaPlayer);
                mediaPlayer.URL = filePath;
                mediaPlayer.Ctlcontrols.stop();

            }
                else if (Path.GetExtension(filePath).Equals(".pdf"))
            {
                AxAcroPDF pdfReader = new AxAcroPDF();
                pdfReader.Height = pnlPreview.Height;
                pdfReader.Width = pnlPreview.Width;
                pdfReader.Name = "PDFReader";
                pnlPreview.Controls.Add(pdfReader);
                pdfReader.src = filePath;
                pdfReader.setShowScrollbars(false);
                pdfReader.setShowToolbar(false);
                pdfReader.setView("fitH");
                pdfReader.setLayoutMode("SinglePage");
                pdfReader.setPageMode("none");
            }
                else if (Path.GetExtension(filePath).Equals(".html") || Path.GetExtension(filePath).Equals(".htm"))
            {
                WebBrowser BrowserObj = new WebBrowser();
                BrowserObj.Height = pnlPreview.Height;
                BrowserObj.Width = pnlPreview.Width;
                BrowserObj.Name = "Html";
                BrowserObj.Navigate(filePath);
                
                pnlPreview.Controls.Add(BrowserObj);

            }
                else
                {
                    Label previewNotFound = new Label();
                    previewNotFound.Location = new Point(150,310);
                    previewNotFound.Text = "Peview Not Found";
                    pnlPreview.Controls.Add(previewNotFound);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Fehler Meldung");
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if(txtSearchFile.Text == "")
            {
                MessageBox.Show("Geben Sie name von Suche Datei ein!","Info");
            }
            else if(currentPath == "")
            {
                MessageBox.Show("Laden Sie erst die Datei hoch!","Info");
            }
            else
            {
                lsvFiles.Items.Clear();
                lsvFiles.View = View.List;

                directoryInfo = new DirectoryInfo(currentPath);
                fileSearch(directoryInfo);
                search = 1;
            }
            
        }

        //http://www.codeproject.com/Articles/14291/Scan-directories-using-recursion
        private void fileSearch(DirectoryInfo directory)
        {
           
            foreach (FileInfo file in directory.GetFiles())
            {
                if (file.Name.Contains(txtSearchFile.Text))
                {
                    Icon iconForFile = SystemIcons.WinLogo;
                    ListViewItem item = new ListViewItem(file.FullName, 1);
                    iconForFile = Icon.ExtractAssociatedIcon(file.FullName);

                    // Check to see if the image collection contains an image
                    // for this extension, using the extension as a key.
                    if (!ilstFileAndIcon.Images.ContainsKey(file.Extension))
                    {
                        // If not, add the image to the image list.
                        iconForFile = System.Drawing.Icon.ExtractAssociatedIcon(file.FullName);
                        ilstFileAndIcon.Images.Add(file.Extension, iconForFile);
                    }
                    item.ImageKey = file.Extension;
                    lsvFiles.Items.Add(item);
                }
            }

            DirectoryInfo[] subDirectories = directory.GetDirectories();
            
            //search all directory of current path
            foreach (DirectoryInfo subDirectory in subDirectories)
            {
                fileSearch(subDirectory);
            }
        }

        private void verToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lsvFiles.Items.Count == 0)
            {
                MessageBox.Show("Laden Sie erst die Datei hoch!","Info");
            }
            else
            {
                if(lsvFiles.SelectedItems.Count == 0)
                {
                    MessageBox.Show("Selektieren Sie erst die Datei!","Info");
                }
                else
                {
                    btnMove_Click(sender, e);
                }
            }            
        }

        private void kopierenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lsvFiles.Items.Count == 0)
            {
                MessageBox.Show("Laden Sie erst die Datei hoch!","Info");
            }
            else
            {
                if (lsvFiles.SelectedItems.Count == 0)
                {
                    MessageBox.Show("Selektieren Sie erst die Datei!","Info");
                }
                else
                {
                    btnCopy_Click(sender, e);
                }
            }
        }

        private void löschenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lsvFiles.Items.Count == 0)
            {
                MessageBox.Show("Laden Sie erst die Datei hoch!","Info");
            }
            else
            {
                if (lsvFiles.SelectedItems.Count == 0)
                {
                    MessageBox.Show("Selektieren Sie erst die Datei!","Info");
                }
                else
                {
                    btnDelete_Click(sender, e);
                }
            }
        }

        private void annotierenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lsvFiles.Items.Count == 0)
            {
                MessageBox.Show("Laden Sie erst die Datei hoch!","Info");
            }
            else
            {
                if (lsvFiles.SelectedItems.Count == 0)
                {
                    MessageBox.Show("Selektieren Sie erst die Datei!","Info");
                }
                else
                {
                    btnEdit_Click(sender, e);
                }
            }
        }

        private void öffnenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnUpload_Click(sender, e);
        }

        private void textDokumenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (currentPath=="")
            {
                MessageBox.Show("Laden Sie erst die Datei hoch!","Info");
            }
            else
            {
                filterFiles("text", lsvFiles);
            }
        }

        private void audioFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (currentPath == "")
            {
                MessageBox.Show("Laden Sie erst die Datei hoch!","Info");
            }
            else
            {
                filterFiles("audio", lsvFiles);
            }
        }

        private void videoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (currentPath == "")
            {
                MessageBox.Show("Laden Sie erst die Datei hoch!","Info");
            }
            else
            {
                filterFiles("video", lsvFiles);
            }
        }

        private void bilderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (currentPath == "")
            {
                MessageBox.Show("Laden Sie erst die Datei hoch!","Info");
            }
            else
            {
                filterFiles("picture", lsvFiles);
            }
        }

        private void andereToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (currentPath == "")
            {
                MessageBox.Show("Laden Sie erst die Datei hoch!","Info");
            }
            else
            {
                filterFiles("mix", lsvFiles);
            }
        }
    }
}